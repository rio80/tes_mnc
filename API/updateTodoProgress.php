<?php
$method = isset($_SERVER['REQUEST_METHOD']);
if ($method == 'GET') {
    require_once '../koneksi.php';
    $query    = "";
    $response = array();
    $idTodo   = isset($_GET['idtodo']) ? $_GET['idtodo'] : "";
    $progress = isset($_GET['progress']) ? $_GET['progress'] : "";

    if (empty($idTodo) || empty($progress)) {
        $response['value']   = "0";
        $response['message'] = "Value error atau masih kosong";
    } else {
        $query = "UPDATE todo_list SET progress=$progress
					WHERE idTodo=$idTodo";

        $exec = $con->query($query);

        if ($exec) {
            $response['value']   = "1";
            $response['message'] = "progress dengan id " . $idTodo . " berhasil update";
        } else {
            $response['value']   = "0";
            $response['message'] = "progress dengan id " . $idTodo . " gagal update";
        }

    }
    echo json_encode($response);
}
