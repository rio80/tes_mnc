﻿# Host: localhost  (Version 5.5.5-10.1.25-MariaDB)
# Date: 2018-01-12 14:59:42
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "login"
#

DROP TABLE IF EXISTS `login`;
CREATE TABLE `login` (
  `idLogin` int(8) NOT NULL AUTO_INCREMENT,
  `adminName` varchar(8) NOT NULL DEFAULT '-',
  `password` varchar(8) NOT NULL DEFAULT '-',
  PRIMARY KEY (`idLogin`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

#
# Data for table "login"
#

INSERT INTO `login` VALUES (1,'sa','fdf'),(2,'rio','1234');

#
# Structure for table "todo_list"
#

DROP TABLE IF EXISTS `todo_list`;
CREATE TABLE `todo_list` (
  `idTodo` int(8) NOT NULL AUTO_INCREMENT,
  `idUser` int(8) NOT NULL DEFAULT '0',
  `TaskName` varchar(200) NOT NULL DEFAULT '-',
  `progress` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idTodo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

#
# Data for table "todo_list"
#

INSERT INTO `todo_list` VALUES (1,1,'Kerjain Front end company profile',0),(2,2,'Setting windows server',40),(3,2,'setting Microtik',0),(4,2,'Crimping kabel LAN',0),(5,1,'Ganti kode Debug',0),(6,1,'benerin kabel yang berantakan',0),(7,1,'tes dulu',0);

#
# Structure for table "user"
#

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `idUser` int(8) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(8) NOT NULL DEFAULT '-',
  `division` varchar(8255) NOT NULL DEFAULT '-',
  PRIMARY KEY (`idUser`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

#
# Data for table "user"
#

INSERT INTO `user` VALUES (1,'Budi','Programmer'),(2,'Adi','Teknisi');
